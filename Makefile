MODULE_SRC:=./src
PREFIX?=/usr/bin

BINARY:=./bin/rtbt.bin
INSTALL = install -m 755

.PHONY: all clean debug distclean release

all:
	$(MAKE) -C $(MODULE_SRC) all

clean:
	-rm modules.order
	-rm Module.symvers
	$(MAKE) -C $(MODULE_SRC) clean

debug:
	$(MAKE) -C $(MODULE_SRC) debug

distclean:
	$(MAKE) -C $(MODULE_SRC) distclean

release:
	$(MAKE) -C $(MODULE_SRC) release

install_module:	
	$(MAKE) -C $(MODULE_SRC) install

install_binary:	$(BINARY)
	$(INSTALL) $(BINARY) $(PREFIX)/rtbt.bin
	@echo Remember you need to run $(PREFIX)/rtbt.bin
	@echo to use the device.


uninstall_module:	
	$(MAKE) -C $(MODULE_SRC) uninstall

uninstall_binary:	$(BINARY)
	-rm $(PREFIX)/rtbt.bin

install: install_module install_binary

uninstall: uninstall_module uninstall_binary

