# About
This is a patched version of the ralink rt3290 bluetooth card.
The original sources are taken from [here](http://downloads.zotac.com/mediadrivers/mb/download/NB087_Ubuntu.zip).
I am not a kernel expert, I will try to keep this stable and updated for my own good and for everyone else who is interested, but I cannot promise stability and safety.
If you experience issues feel free to contact me with a detailed report, but again I cannot promise a solution.

# Build and install
Build should be as easy as
~~~~
$ make
~~~~

If everything is fine, you can install the module by
~~~~
$ sudo make install
~~~~

For the bluetooth to work you also need an userland process and a character file in /dev. The script `./scripts/initbt.sh` takes care of both tasks:

- The userland process is provided in the original (unpatched) driver and has no source code, at least that I can find. Trust it at your own risk.
It will be installed at `/usr/bin/rtbt.bin`, unles other `PREFIX` is specified.

- The character file needs to be created at startup: `# mknod /dev/rtbth c 192 0`

As a reference, the systemd unit I am using is `./scripts/bluetoothdrv.service`. As the way a daemon is run depends on your distribution, the installer DOES NOT install the systemd unit and `./scripts/initbt.sh`. You must provide a way to run the daemon.

An installer for systemd is planned.

# Known issues
- Kernel panic when the module is unloaded with rmmod. If you need to get rid of it uninstall it and reboot the machine.